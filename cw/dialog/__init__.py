#!/usr/bin/env python
# -*- coding: utf-8 -*-

from . import message
from . import cardinfo
from . import cardcontrol
from . import select
from . import scenarioselect
from . import scenarioinstall
from . import charainfo
from . import text
from . import create
from . import edit
from . import etc
from . import settings
from . import skin
from . import skininstall
from . import partyrecord
from . import transfer
from . import progress


def main():
    pass


if __name__ == "__main__":
    main()
